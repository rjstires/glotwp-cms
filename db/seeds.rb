Faker::Config.locale = 'en-US'

@category_duty_officer = Category.create!(name: 'duty officer')
  Card.create!(
    title: Faker::Name.name, 
    category: @category_duty_officer,
    p_1: Faker::PhoneNumber.phone_number,
    p_2: Faker::PhoneNumber.cell_phone,
    active: true,
    )
4.times do
  Card.create!(
    title: Faker::Name.name, 
    category: @category_duty_officer,
    p_1: Faker::PhoneNumber.phone_number,
    p_2: Faker::PhoneNumber.cell_phone,
    )
end

@category_duty_detective = Category.create!(name: 'duty detective')
  Card.create!(
    title: Faker::Name.name, 
    category: @category_duty_detective,
    p_1: Faker::PhoneNumber.phone_number,
    p_2: Faker::PhoneNumber.cell_phone,
    active: true,
    )
4.times do
  Card.create!(
    title: Faker::Name.name, 
    category: @category_duty_detective,
    p_1: Faker::PhoneNumber.phone_number,
    p_2: Faker::PhoneNumber.cell_phone,
    )
end

@category_towing = Category.create!(name: 'on call towing')
  Card.create!(
    title: Faker::Company.name, 
    category: @category_towing,
    p_1: Faker::PhoneNumber.phone_number,
    p_2: Faker::PhoneNumber.cell_phone,
    active: true,
    )
4.times do
  Card.create!(
    title: Faker::Company.name, 
    category: @category_towing,
    p_1: Faker::PhoneNumber.phone_number,
    p_2: Faker::PhoneNumber.cell_phone,
    )
end

@category_court_clerk = Category.create!(name: 'court clerk')
  Card.create!(
    title: Faker::Name.name, 
    category: @category_court_clerk ,
    p_1: Faker::PhoneNumber.phone_number,
    p_2: Faker::PhoneNumber.cell_phone,
    active: true,
    )
4.times do
  Card.create!(
    title: Faker::Name.name, 
    category: @category_court_clerk ,
    p_1: Faker::PhoneNumber.phone_number,
    p_2: Faker::PhoneNumber.cell_phone,
    )
end

@category_oem = Category.create!(name: 'emergency management')
  Card.create!(
    title: Faker::Name.name, 
    category: @category_oem,
    p_1: Faker::PhoneNumber.phone_number,
    p_2: Faker::PhoneNumber.cell_phone,
    active: true,
    )
4.times do
  Card.create!(
    title: Faker::Name.name, 
    category: @category_oem,
    p_1: Faker::PhoneNumber.phone_number,
    p_2: Faker::PhoneNumber.cell_phone,
    )
end
