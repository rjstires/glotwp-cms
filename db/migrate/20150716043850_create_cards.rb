class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.references :category, index: true, foreign_key: true, null: false
      t.boolean :cards, :active, :boolean, default: false
      t.string :title, null: false
      t.string :p_1, null: false
      t.string :p_2
      t.string :p_3
      t.string :p_4
      t.string :p_5

      t.timestamps null: false
    end
  end
end
