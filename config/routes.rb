Rails.application.routes.draw do
  resources :cards
  resources :categories

  root 'home#index'
  post '/' => 'cards#set_active'
end
