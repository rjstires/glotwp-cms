module CardsHelper
  
  def p_row(text)
    "<p class=\"list-group-item-text\">#{text}</p>".html_safe unless text == ''
  end
  
  def render_card(card)
    unless card.blank?
      render partial: '/cards/card', locals: {card: card}
    end
  end
  
  def display_active_star(card)
    "<i class=\"fa fa-lg fa-star\"></i>".html_safe if(card.active)
  end
    
end
