class Category < ActiveRecord::Base
  has_many :cards, dependent: :destroy
  validates :name, presence: true, uniqueness: true
  def display_name 
    self.name.titleize
  end
  def file_name 
    self.name.downcase.tr(" ", "_")
  end
  
end
