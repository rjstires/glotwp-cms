class Card < ActiveRecord::Base
  belongs_to :category
  before_save :set_all_inactive
  before_save :set_active_if_new_category
    
  scope :active, -> { where(active: true) }
  
  def display_title
    self.title.titleize
  end
  
  def set_all_inactive
    Card.where(:category => self.category).update_all(active: false) if self.active == true
  end
  
  def set_active_if_new_category
    @card = Card.where(:category => self.category).active
    logger.debug "Checking..."
    if @card.empty?
      logger.debug "No ative card, setting this to ture."
      self.active = true
    end
  end
  
end
