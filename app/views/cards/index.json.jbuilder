json.array!(@cards) do |card|
  json.extract! card, :id, :category_id, :title, :p_1, :p_2, :p_3, :p_4, :p_5
  json.url card_url(card, format: :json)
end
