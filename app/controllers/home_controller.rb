class HomeController < ApplicationController
  def index
    @cards = []
    @all_categories = Category.all
    @all_cards = Card.all
    
    @all_categories.each do |category|
      card = Card.includes(:category).where('categories.name' => category.name).active.first
      @cards.push(card) if card
    end
  end
end
